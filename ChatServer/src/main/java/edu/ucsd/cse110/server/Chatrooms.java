/* Julie Anderson PID:A08165435 */

package edu.ucsd.cse110.server;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;


public class Chatrooms
{
	private static Map<String, LinkedList<String>> chatrooms;

	public void initChatrooms(HashMap<String, LinkedList<String>> initializedChatrooms)
	{
		Chatrooms.chatrooms = initializedChatrooms;		
	}
	
	public boolean isUserInChatroom(String Chatroom, String User)
	{
		LinkedList<String> users = chatrooms.get(Chatroom);
		
		Iterator<String> iter = users.iterator();
		while(iter.hasNext())
		{
			if(iter.next().equals(User))
			{
				return true;
			}
		}
		return false;
	}
	
	/*
	 * this fcn assumes user will only be in one chatroom and stops searching once a user
	 * is found in a chatroom
	 */
	public String getUsersChatroom(String User)
	{
		Iterator<String> iterator = chatrooms.keySet().iterator();
		
		while (iterator.hasNext()) 
		{
			String Chatroom = iterator.next().toString();
			LinkedList<String> users = chatrooms.get(Chatroom);
			
			Iterator<String> iter = users.iterator();
			while(iter.hasNext())
			{
				if(iter.next().equals(User))
				{
					return Chatroom;
				}
			}		
		}
		return null;
	}
	
	public String addUserToChatroom(String Chatroom, String User) {
		LinkedList<String> users = chatrooms.remove(Chatroom);
		users.add(User);
		chatrooms.put(Chatroom, users);
		return new String();
	}
	
	public void removeUserFromChatroom(String Chatroom, String User) {
		chatrooms.remove(Chatroom);
	}
	
	public void addChatroom(String Chatroom)
	{
		chatrooms.put(Chatroom, new LinkedList<String>());
	}
	
	public String printAllChatrooms() {
		
		Iterator<String> iterator = chatrooms.keySet().iterator();
		StringBuilder allChatrooms = new StringBuilder();

		while (iterator.hasNext()) 
		{
			String Chatroom = iterator.next().toString();
			LinkedList<String> users = chatrooms.get(Chatroom);
			
			Iterator<String> iter = users.iterator();
			while(iter.hasNext())
			{
				String chatroom = iterator.next().toString();
				allChatrooms.append(chatroom);
			}		
		}
		return allChatrooms.toString();
	}
}