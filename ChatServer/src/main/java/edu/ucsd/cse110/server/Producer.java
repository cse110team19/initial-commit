/* Julie Anderson PID:A08165435 */

package edu.ucsd.cse110.server;
import java.net.URISyntaxException;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;


public class Producer
{
	static public MessageProducer producer;
	static public Session session;

	public void initProducer() throws JMSException, URISyntaxException  
	{
		try
		{
			ActiveMQConnection connection = 
					ActiveMQConnection.makeConnection(
							Constants.ACTIVEMQ_URL);
			Producer.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue inputQueue = Producer.session.createQueue(Constants.CLIENT_QUEUE);
			Producer.producer = Producer.session.createProducer(inputQueue);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	
	public void setProducer(String queuename) throws JMSException
	{
		Queue inputQueue = Producer.session.createQueue(queuename);//(parseUsername(msg));
		Producer.producer = Producer.session.createProducer(inputQueue);
	}
	
	public MessageProducer getProducer()
	{
		MessageProducer producer = Producer.producer;
		return producer;
	}
	
	public Session getSession()
	{
		Session session = Producer.session;
		return session;
	}
}