package edu.ucsd.cse110.server;

public interface Constants {
	public static String ACTIVEMQ_URL = "tcp://localhost:61615";
	public static String USERNAME = "max";	
	public static String PASSWORD = "pwd";	
	public static String QUEUENAME = "test";
	public static String CLIENT_QUEUE = "client";
	public static String ONLINEUSERS = "online";
}