package edu.ucsd.cse110.server;

import java.util.LinkedList;


public class Command {
	public String msg;
	
	public Command(String msg)
	{
		this.msg = msg;
	}
	
	public String getCommand()
	{
		String command = new String();
		int i;
		for(i = 0; i < msg.length(); i++)
		{
			if( msg.charAt(i) == ':' )
				break;
			command += msg.charAt(i);
		}

		return command;
	}
	

	public String getUsername()
	{
		String command = new String();
		int i;
		for(i = 0; i < msg.length(); i++)
		{
			if( msg.charAt(i) == ':' )
				break;
		}
		
		i++;
		
		for(; i < msg.length(); i++)
		{
			if( msg.charAt(i) == ':' )
				break;
			command += msg.charAt(i);
		}
		
		return command;
	}
	
	
	public String getPassword(){
		String command = new String();
		int i;
		for( i = 0;  i < msg.length(); i++){
			if( msg.charAt(i) == ':' )
				break;
		}
		i++;
		
		for(; i < msg.length(); i++)
		{
			if( msg.charAt(i) == ':' )
				break;
		}
		
		i++;
		
		for(; i < msg.length(); i++)
		{
			if( msg.charAt(i) == ':')
				break;
			command += msg.charAt(i);
		}
		return command;
		}
	
	// gets the number of arguments
	public int getNumArgs()
	{
		int numArgs = 0;
		int i;
		for(i = 0; msg.charAt(i) != ':'; i++);
		
		if(msg.charAt(++i) != '\n')
		{
			numArgs++;
		}
		else
		{
			return 0;
		}
		
		for(;i < msg.length(); i++)
		{
			if( msg.charAt(i) == '\n' )
				break;
			if(msg.charAt(i) == ',')
			{
				numArgs++;
			}
		} 
		return numArgs;
	}
	
	// for private message command, parse the list of users
	public LinkedList<User> getListOfUsers(){
		String list = new String();
		User userObj;
		LinkedList<User> listOfUsers = new LinkedList<User>();
		
		//go through the original message and stop at colon
		int i;
		for(i = 0; i < msg.length(); i++)
		{
			if( msg.charAt(i) == ':' )
				break;
		}
		
		// save the list of users string after the colon
		for(++i;i < msg.length(); i++){
			list += msg.charAt(i);
		}
		
		String user = new String("");

		// go through the list of users and add the users to the linked list
		for(i = 0; i < list.length(); i++){
			if(msg.charAt(i) != ',' && msg.charAt(i) != ':'){
				user += msg.charAt(i);
			}
			else if(msg.charAt(i) == ','){
				userObj = new User(user, "");
				listOfUsers.add(userObj);
				i+=2;
			}
			else{
				break;
			}
		}
		
		return listOfUsers;
	}
	
	// get the message content
	public String getMessage(){
		String command = new String();
		int i;
		for(i = 0; i < msg.length(); i++)
		{
			if( msg.charAt(i) == ':' )
				break;
			command += msg.charAt(i);
		}

		return command;
	}
	
	public String getChatroom(){
		return null;
	}

	public String getPrivateMessage() {
		String list = new String();
		String message = new String();
		int i = 0;
		
		// go through the list of users and add the users to the linked list
		for(i = 0; i < list.length(); i++){
			if(msg.charAt(i) != ',' && msg.charAt(i) != ':'){
			}
			else if(msg.charAt(i) == ','){
				i+=2;
			}
			else{
				for(++i; i < msg.length(); i++)
					message += msg.charAt(i);
			}
		}
		return message;
	}
}