/* Julie Anderson PID:A08165435 */

package edu.ucsd.cse110.server;
import java.util.*;



public class UsernameDB
{
	static protected LinkedList<User> usernameDatabase = new LinkedList<User>();

	public void addUser(User user) {
		usernameDatabase.add(user);
	}
	
	public void removeUser(User user) {
		usernameDatabase.remove(user);
	}
	
	public void logout(User user)
	{
		Iterator<User> iter = usernameDatabase.iterator();
		while(iter.hasNext()) {
			User user2 = iter.next();
			if(user2.equals(user))
			{
				user2.setStatus(false);
				break;
			}
		}
	}
	
	public void login(User user)
	{
		Iterator<User> iter = usernameDatabase.iterator();
		while(iter.hasNext()) {
			User user2 = iter.next();
			if(user2.equals(user))
			{
				user2.setStatus(true);
				break;
			}
		}
	}
	
	public String printAllUsers() {
		String allUsers = "";
		Iterator<User> iter = UsernameDB.usernameDatabase.iterator();
		while(iter.hasNext()) {
			User user = iter.next();
			if(user.getStatus()){
				allUsers += user.getUsername(); 
				allUsers += ", ";
			}
		}
		System.out.println("database about to print :" + 
				allUsers.substring(0,allUsers.length()-2));
		return allUsers.substring(0, allUsers.length()-2);
	}
	
	public String sendUserList(){
		String allUsers = "";
		Iterator<User> iter = UsernameDB.usernameDatabase.iterator();
		while(iter.hasNext()) {
			User user = iter.next();
			allUsers += user.getUsername(); 
			allUsers += ",";
		}
		System.out.println("database about to print :" + allUsers);
		return allUsers;
	}
	
	public boolean checkUser(User user)
	{
		Iterator<User> iter = usernameDatabase.iterator();
		while(iter.hasNext()) {
			if(iter.next().equals(user))
				return true;
		}
		return false;
	}
	
	public boolean correctLogin(User user)
	{
		Iterator<User> iter = usernameDatabase.iterator();
		while(iter.hasNext()) {
			User user2 = iter.next();
			System.out.println("You are comparing " + user + " with " + user2 );
			 if(user2.equals(user))
				 return true;
		}
		return false;
	}
	
	public boolean checkIfOnline(User user){
		Iterator<User> iter = usernameDatabase.iterator();
		User userTest = new User("", "");
		
		while(iter.hasNext()) {
			userTest = iter.next();
			if(userTest.equals(user) && userTest.getStatus()){
				return true;
			}
			else{
				return false;
			}
		}
		return false;
	}
	
	
	public LinkedList<User> returnList()
	{
		return new LinkedList<User>(usernameDatabase);
	}
}