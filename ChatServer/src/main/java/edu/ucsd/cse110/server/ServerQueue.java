package edu.ucsd.cse110.server;

import java.util.ArrayList;
import java.util.List;

public class ServerQueue {
	private List<String> serverLog = new ArrayList<String>();

	static private ServerQueue serverQ = new ServerQueue();

	// prevent instantiation from other classes
	private ServerQueue(){};
	
	
	// singleton pattern here
	static public ServerQueue getInstance() {
		return serverQ;
	}
	
	public void addMessage(String entry) {
		serverLog.add(entry);
	}
	
	public String getMessage() {
		String logImage = "";
		for (String logEntry : serverLog) {
			if (logImage.length() != 0) {logImage += "\n";}
			logImage += logEntry;
		}
		return logImage;
	}

}

