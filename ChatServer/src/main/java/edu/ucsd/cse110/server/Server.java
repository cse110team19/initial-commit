package edu.ucsd.cse110.server;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import edu.ucsd.cse110.server.Constants;
//import edu.ucsd.cse110.server.ChatClientApplication.CloseHook;

@SuppressWarnings("unused")
public class Server {
	
    UsernameDB database = new UsernameDB();
    HashMap<String, MessageProducer> producerList = new HashMap<String, MessageProducer>();
    LinkedList<User> usernameDB;
	private Session session;
	
	// return true if the message is a command
    private boolean isMsgCmd(String msg)
    {
    	if(msg.charAt(0) == '$')
    	{
    		return true;
    	}
    	return false;
    }
    
    // execute the command accordingly
    private String executeCmd(String msg)
    {
    	Command command = new Command(msg);
        User user = new User(command.getUsername(), command.getPassword());
        Chatrooms chatrooms = new Chatrooms();
        
        switch(checkCommand(command)){
        	case 0: // add new user
        		addNewUser(database, user);
        		break;
        	case 1: // print users online
        		return database.printAllUsers();
        	case 2: // check login
        		return checkUserLogin(database, user);
        	case 3: // logout
        		database.logout(user);
        		break;
        	case 4: //login
				database.login(user);
				break;
        	case 5: // print chatrooms
        		return chatrooms.printAllChatrooms();
        	case 6:	// subscribe to a chatroom
        		return chatrooms.addUserToChatroom(command.getChatroom(), user.getUsername());
        	case 7: // private message a given list of users
        		System.out.println("WE ARE IN THE PRIVATE MESSAGE CASE");
        		String cmdName = command.getCommand();
        		LinkedList<User> listOfUsers = command.getListOfUsers();
        		String privMessage = command.getPrivateMessage();
        		privateMessage(cmdName, listOfUsers, command, privMessage);
        		break;
        	case 8: //check if username exists in database before adding them
        		System.out.println("Serverlist: " + database.sendUserList());
        		return "$list:" + database.sendUserList();
        	case 9:
        		System.out.println("Could not parse command");
        }
    	return new String("");

    }

	// determine the command sent in the message and execute it
	private int checkCommand(Command command) {
		System.out.println("The command is: " + command.getCommand());
		if(command.getCommand().equals("$newUser"))
    	{
			return 0;
    	}
    	else if(command.getCommand().equals("$printUsersOnline"))
    	{
    		return 1;
    	}
    	else if( command.getCommand().equals("$checkLogin")){
    		return 2;
    	} 
    	else if( command.getCommand().equals("$logout"))
    	{
    		return 3;
    	}
    	else if ( command.getCommand().equals("$login"))
    	{
    		return 4;
    	}
    	else if (command.getCommand().equals("$printChatrooms"))
    	{
    		return 5;
    	}
    	else if( command.getCommand().equals("$subscribeChatroom"))
    	{
    		return 6;
    	}
    	else if(command.getCommand().equals("$private"))
    	{
    		System.out.println("WE ARE DOING THE PRIVATE OPERATION");
    		return 7;
    	}
    	else if(command.getCommand().equals("$allUsers")){
    		System.out.println("ALL USERS COMMAND");
    		return 8;
    	}
    	else
    	{
    		return 9;
    	} 
	}

	// checks if the user exists in the database 
	// if so, then it sets the static variable "check"
	// to true. else false.
	private String checkUserLogin(UsernameDB database, User user) {
		//check if the signing in information is correct
		boolean check = database.correctLogin(user);
		if(check){
			return "true";
		}
		else
			return "false";
	}

	// add a new user to the database
	private void addNewUser(UsernameDB database, User user) {
		
		database.addUser(user);
	}

	
	// message all users listed
    private void privateMessage(String cmdName, LinkedList<User> listOfUsers, Command command, String msg) {
		// incorrect message right now
		String returnMessage = command.getUsername() + ": " + command.getMessage();
		
		
		// sending to users online and on the list
		for ( User p : listOfUsers){
			if( p.getStatus()){
				// don't send back to original message sender
				if(!(p.equals(command.getUsername())))
					try {
						producerList.get(p).send(this.session.createTextMessage(returnMessage));
					} catch (JMSException e) {
						System.out.println("Could not send messages to the users listed");
						e.printStackTrace();
					}
			}
		}
	}
	
 
    // receives the message from the clients and processes the message according to what it contains
	public void receive(String msg) {
		try
		{
			System.out.println("In server.java printing message: " + msg);
			
			Command command = new Command(msg);
			//Producer producer = new Producer();
			ActiveMQConnection connection = 
					ActiveMQConnection.makeConnection( command.getUsername(), null, 
							Constants.ACTIVEMQ_URL);
			this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue inputQueue = session.createQueue(command.getUsername());
			MessageProducer producer = session.createProducer(inputQueue);
			String returnString = new String();
			
			
			if (checkCommand(command)==0)
			{
				//MessageProducer producer = session.createProducer(inputQueue);
				producerList.put(command.getUsername(), producer);
			}

			// if the message is a command
			if(isMsgCmd(msg))
			{
				Command com = new Command(msg);
				if(msg!=null){
					if(producerList.get(com.getUsername()) != null){
						returnString = executeCmd(msg);
					}
					else{
						returnString = executeCmd(msg);
					}
					producer.send(session.createTextMessage(returnString));
				}

			}
			// if message is not a command then we will broadcast the message to all online users
			else
			{		
				Command com = new Command(msg);
				String returnMessage = com.getUsername() + ": " + com.getMessage();
				usernameDB = database.returnList();
				
				
				// sending to all users online
				for ( User p : usernameDB ){
					if( p.getStatus()){
						// don't send back to original message sender
						if(!(p.getUsername().equals(com.getUsername())))
							producerList.get(p.getUsername()).send(session.createTextMessage(returnMessage));
					}
				}
				
				producer.send(session.createTextMessage(returnString));
			}
		}
		// catch an exception if something goes wrong 
		catch(Exception e)
		{		
			System.out.println("Called: " + e.toString());
			e.printStackTrace();
		}
	}
	
}