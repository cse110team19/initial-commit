package edu.ucsd.cse110.client;

public class User {
	
	private String username;
	private String password;
	private boolean status;
	
	public User(String username, String password)
	{
		this.username = username;
		this.password = password;
		status = true;
	}
	
	public String getUsername()
	{
		return username;
	}
	public String getPassword()
	{
		return password;
	}
	public boolean equals(User user)
	{
		System.out.println(user);
		if( username.equals(user.getUsername())){
			if( password.equals(user.getPassword())){
				return true;
			}
		}
		return false;
		
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}