package edu.ucsd.cse110.client;

import java.util.Scanner;

public class ChatClientGui {
	
	public ChatClientGui() {
	} 
	
	public void displayWelcomeScreen() {
		System.out.println("Welcome to the Team 19 chatterbox");
	}
	
	/*
	 * usernames cannot start with $ because we will use messages starting with $ as internal
	 * commands from the client to the server
	 */
	public String getUsername() {
		System.out.println("Please enter your username followed by the return key "
				+ "\nusernames can't start with $ and cannot contain commas");
		Scanner userInput = new Scanner(System.in);
		String userInputStr = userInput.nextLine();
		return userInputStr;		
	}
	
	public String getPassword() {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Please enter your password followed by the return key");
		String userInputStr = userInput.nextLine();
		return userInputStr;	
	}
	
	public String newUser()
	{
		Scanner userInput = new Scanner(System.in);
		System.out.println("Are you a new user? (y/n): ");
		String userInputStr = userInput.nextLine();
		return userInputStr;
	}
	public void displaySignIn() {
		System.out.println("Please sign in.");
	}
	public void displayTryAnother()
	{
		System.out.println("The username you have chosen already exist try another.");
	}
	public void displayIncorrectInfoUsr()
	{
		System.out.println("Please retype your information");
	}
	public String getNextLn(Scanner inputScanner){
		return inputScanner.nextLine();
	}
	
	
	//TODO
	public void printoptions() {
		System.out.println("if you want to see who's online, type $printUsersOnline");
		System.out.println("if you want to private talk, type \n$private: username1, username2, .., usernamen: message");
		System.out.println("if you want to see available chatrooms, type $printChatrooms");
		System.out.println("if you want to connect to a chatroom, type $subscribeChatroom");
		System.out.println("if you want to logout of server, type $logout");

	}
	
}