package edu.ucsd.cse110.client;


import java.net.URISyntaxException;
import java.util.*;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;


public class ChatClientApplication {

	/*
	 * This inner class is used to make sure we clean up when the client closes
	 */
	static private class CloseHook extends Thread {
		ActiveMQConnection connection;
		private CloseHook(ActiveMQConnection connection) {
			this.connection = connection;
		}
		
		public static Thread registerCloseHook(ActiveMQConnection connection) {
			Thread ret = new CloseHook(connection);
			Runtime.getRuntime().addShutdownHook(ret);
			return ret;
		}
		
		public void run() {
			try {
				//set boolean value of user to false
				System.out.println("Closing ActiveMQ connection");
				connection.close();
			} catch (JMSException e) {
				/* 
				 * This means that the connection was already closed or got 
				 * some error while closing. Given that we are closing the
				 * client we can safely ignore this.
				*/
			}
		}
	}

	/*
	 * This method wires the client class to the messaging platform
	 * Notice that ChatClient does not depend on ActiveMQ (the concrete 
	 * communication platform we use) but just in the standard JMS interface.
	 */
	private static ChatClient wireClient(String username, String password) throws JMSException, URISyntaxException {
		ActiveMQConnection connection = 
				ActiveMQConnection.makeConnection(
						username, password, Constants.ACTIVEMQ_URL);
        connection.start();
        CloseHook.registerCloseHook(connection);
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue destQueue = session.createQueue(Constants.QUEUENAME);
        Queue inputQueue = session.createQueue(username);
        MessageProducer producer = session.createProducer(destQueue);
        
        MessageConsumer consumer = session.createConsumer(inputQueue);
        
        return new ChatClient(producer,session, connection, consumer);
	}
	
	
	// send a command which will be executed accordingly by server
	private static void sendCmd(ChatClient client, String command, String username, String password) throws JMSException
	{
		String fullmessage = new String();
		fullmessage = command + ":" + username + ":" + password;
		try
		{
			client.send(fullmessage);
		}
		catch(JMSException e)
		{
			System.out.println(e.toString());
		}
	}
		
	public static void main(String[] args) {
		try {
			ChatClientGui gui = new ChatClientGui();
			gui.displayWelcomeScreen();
			String username;
			String pswd;
				
			
			/* 
			 * We have some other function wire the ChatClient 
			 * to the communication platform
			 */

			ChatClient client = null;
			ChatClient dummy = wireClient("$dummy", "");
			Scanner userInput = new Scanner(System.in);
			StringTokenizer token;
			
		       String answer = gui.newUser();
		       //if user wants to register for first time type y or yes
		       if( answer.equals("y") || answer.equals("Y"))
		       {
		    	   username = gui.getUsername();
		    	   pswd = gui.getPassword();   
		    	   
		    	   //return list of users from database
		    	   sendCmd(dummy, "$allUsers", "$dummy", "");
		    	   dummy.receive();
		    	   String list = dummy.getList();
		    	   System.out.println("List: " + list);
		    	   
		    	   boolean loopVar = true;
		    	   boolean addUser = true;
		    	   
		    	   if(list != null){
		    	   // loop until valid username is entered
		    	   do{
		    		   token = new StringTokenizer(list, ",");
		    		   // while we have more usernames to check in list
		    		   while(token.hasMoreTokens()){
		    			   loopVar = true;
		    			   // if the username exists in the list: prompt user again for input
		    			   if(token.nextToken().equals(username)){
		    				   loopVar = false;
		    				   	System.out.println("Already existing user. Please input another username.");
		    				   break;
		    			   }
		    		   }
		    		   // prompt user for input
		    		   if(loopVar == false){
		    			   username = gui.getUsername();
		    			   pswd = gui.getPassword();
		    		   }
		    		   // exit entire loop and continue with creating client for valid username, password
		    		   else{
		    			   addUser = false;
		    		   }
		    	   }while(addUser);
		    	   }
		    	   
		    	   dummy.connection.close();
		    	   
		    	   // create client with valid username, pswd
				   client = wireClient(username, pswd);
				   
				   // send command to server to allow it to handle adding user to database 
			       sendCmd(client, "$newUser", username, pswd);
			       
			       // receive feedback from user after sending newUser command
			       client.receive();
			       
			       // let user know that they are logged in successfully
			       System.out.println("You are now logged in");
			       
			       // print out command options available for user
			       gui.printoptions();
		       }
		       else{ 
		    	   //once logged in
		    	   gui.displaySignIn();
		       
		    	   username = gui.getUsername();
		    	   pswd = gui.getPassword();			
		    	   boolean login = false;
		    	   do{

		    		   client = wireClient(username, pswd);
		    		   sendCmd(client, "$checkLogin", username, pswd);
		    		   client.receive(); //delay issue with receive
		    		   boolean bool = ChatClient.getCheckLogin();
		    		   if(bool){
		    			   login = true;
		    			   sendCmd(client, "$login", username, pswd);
		    			   client.receive();
		    			   System.out.println("Successful login");
		    			   gui.printoptions();
		    		   }
		    		   else{
		    			   gui.displayIncorrectInfoUsr();
		    			   username = gui.getUsername();
		    			   pswd = gui.getPassword();
		    			   client.connection.close();
		    		   }
		    	   }while( login == false ); 
		    	   System.out.println("ChatClient wired.");
		       }
		       String command;
	        do
	        {
	        	// we need to change the function below to use 
	        	//scanf to get new user input and send that instead of sending the 
	        	//username each time
	        	System.out.print("Me: ");
	        	command = userInput.nextLine();
	        	
	        	/*
	        	 *  Added new method sendMessage
	        	 *  noticed in sendCmd it added a $ to beginning of command String
	        	 *  moved if else statement to fit input
	        	 *  
	        	 */
	        	
	        	if( !(command.equals("")))
	        	{
	        		sendCmd(client, command, username, pswd);
		        	client.receive();
	        	}
	        	else
	        	{
	        		client.receive();
	        	}
	        	
	        	if(command.equals("$logout"))
	        	{
	        		System.out.println("You have logged out succesfully.");
	        		userInput.close();
	        		System.exit(1);
	        	}
	        	
	    	} while(true);

		       } catch (JMSException e) {
			
			e.printStackTrace();
		} catch (URISyntaxException e) {
			
			e.printStackTrace();
		}

	}

}
