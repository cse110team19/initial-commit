package edu.ucsd.cse110.client;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.command.ActiveMQTextMessage;

public class ChatClient {
	private MessageProducer producer;
	private MessageConsumer consumer;
	private Session session;
	public ActiveMQConnection connection;
	private static boolean check;
	private String usersList = "empty";
	public ChatClient(MessageProducer producer, Session session, ActiveMQConnection connection, MessageConsumer consumer) {
		super();
		this.producer = producer;
		this.consumer = consumer;
		this.session = session;
		this.connection = connection;
	} 
	
	public void send(String msg) throws JMSException {
		producer.send(session.createTextMessage(msg));
	}
	
	public void receive() throws JMSException {
		try 
		{   
            String strReceive = ((ActiveMQTextMessage) consumer.receive()).getText();
            String subString = new String("");
            for( int i = 0; i < strReceive.length(); i++ ){
            	if(strReceive.charAt(i) != ':'){
            		subString += strReceive.charAt(i);
            	}
            	else{
            		break;
            	}
            }        
			if( strReceive.equals("true")) {
				ChatClient.check = true;
			}
			else if (strReceive.equals("false")){
				ChatClient.check = false;
			}
			else if(subString.equals("$list")){
				if( 6 <= strReceive.length())
					usersList = strReceive.substring(6);
			}
			else{
				System.out.println(strReceive);
			}
		}
		catch(JMSException e)
		{
			System.out.println(e.toString());
		}
	}

	public ActiveMQConnection getConnection() {
		ActiveMQConnection connection = this.connection;
		return connection;
	}
	
	public static boolean getCheckLogin(){
		return check;
	}

	public String getList() {
		return usersList;
	}
	
}
